wsclean (3.4-3) unstable; urgency=medium

  * Fix missing include required for gcc14 in wgridder (Closes: #1075643)

 -- Ole Streicher <olebole@debian.org>  Tue, 30 Jul 2024 18:21:10 +0200

wsclean (3.4-2) unstable; urgency=medium

  * Link Radler a static instead of shared library. Closes: #1070237
  * Push Standards-Version to 4.7.0. No changes needed
  * Add superficial autopkgtest

 -- Ole Streicher <olebole@debian.org>  Sat, 18 May 2024 17:35:57 +0200

wsclean (3.4-1) unstable; urgency=medium

  * New upstream version 3.4
  * Rediff patches. Drop fix-gcc-13.patch: applied upstream
  * Mark libradler shared lib as not installed

 -- Ole Streicher <olebole@debian.org>  Thu, 26 Oct 2023 12:50:06 +0200

wsclean (3.1-4) unstable; urgency=medium

  * Team upload.
  * Add fix-gcc-13.patch to fix gcc-13 build issues. (Closes: #1037899)

 -- Bo YU <tsu.yubo@gmail.com>  Wed, 26 Jul 2023 16:43:38 +0800

wsclean (3.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + wsclean-dev: Add Multi-Arch: same.

  [ Ole Streicher ]
  * Push Standards-Version to 4.6.2, no changes needed
  * Use system provided pybind11

 -- Ole Streicher <olebole@debian.org>  Thu, 05 Jan 2023 12:54:39 +0100

wsclean (3.1-2) unstable; urgency=medium

  * Link libatomic against wsclean-lib on riscv64 and fix FTBFS
    (Closes: #1018157)

 -- Ole Streicher <olebole@debian.org>  Sat, 27 Aug 2022 18:43:24 +0200

wsclean (3.1-1) unstable; urgency=medium

  * New upstream version 3.1
  * Rediff patches
  * Update build dependencies
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 24 May 2022 15:46:46 +0200

wsclean (3.0-2) unstable; urgency=medium

  * Don't try SIMD on non-x86 processors (not implemented yet)
  * Update project homepage

 -- Ole Streicher <olebole@debian.org>  Tue, 02 Nov 2021 08:25:55 +0100

wsclean (3.0-1) unstable; urgency=medium

  * Switch to gitlab.com. d/watch doesn't work anymore, however.
  * New upstream version 3.0. Closes: #997292
  * Rediff patches
  * Push Standards-Version to 4.6.0. No changes required
  * Adjust build dependencies
  * Don't build the shared library anymore
  * Remove outdated license statement

 -- Ole Streicher <olebole@debian.org>  Mon, 01 Nov 2021 09:27:20 +0100

wsclean (2.10.1-1) unstable; urgency=low

  * New upstream version 2.10.1. Fixes FTBFS on 32-bit archs

 -- Ole Streicher <olebole@debian.org>  Mon, 20 Jul 2020 12:24:54 +0200

wsclean (2.10-1) unstable; urgency=low

  * New upstream version 2.10. Rediff patches
  * Add 'Rules-Requires-Root: no' to d/control

 -- Ole Streicher <olebole@debian.org>  Fri, 17 Jul 2020 10:50:25 +0200

wsclean (2.9-2) unstable; urgency=low

  * No-change source-only re-upload

 -- Ole Streicher <olebole@debian.org>  Tue, 28 Apr 2020 08:37:25 +0200

wsclean (2.9-1) unstable; urgency=low

  * New upstream version 2.9. Rediff patches
  * Push Standards-Version to 4.5.0. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Mon, 27 Apr 2020 13:57:26 +0200

wsclean (2.8-1) unstable; urgency=low

  * New upstream version 2.8. Rediff patches
  * Add gitlab-ci.yml for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 20 Sep 2019 11:20:08 +0200

wsclean (2.7-1) unstable; urgency=low

  [ Helmut Grohne ]
  * Fix FTCBFS: Fix build/host confusion. (Closes: #929757)

  [ Ole Streicher ]
  * New upstream version 2.7. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required.
  * Push compat to 12; switch to debhelper-compat
  * Add HDF5 dependency

 -- Ole Streicher <olebole@debian.org>  Thu, 25 Jul 2019 13:12:28 +0200

wsclean (2.6-1) unstable; urgency=low

  * Push Standards-Versionto 4.2.0. No changes required
  * Switch back to unstable

 -- Ole Streicher <olebole@debian.org>  Mon, 06 Aug 2018 12:07:06 +0200

wsclean (2.6-1~exp1) experimental; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 2.6. Rediff patches
  * Push Standards-Version to 4.1.4. No changes needed.
  * Push compat to 11
  * New soname. Upload to experimental.

 -- Ole Streicher <olebole@debian.org>  Mon, 11 Jun 2018 17:53:20 +0200

wsclean (2.5-1) unstable; urgency=medium

  * New upstream version 2.5
  * Rediff patches
  * Push Standards-Version to 4.1.2. Change URLs to https

 -- Ole Streicher <olebole@debian.org>  Sun, 03 Dec 2017 14:03:48 +0100

wsclean (2.4-1) unstable; urgency=low

  * New upstream version 2.4
  * Rediff patches
  * Push Standards-Version to 4.0.0. No changes

 -- Ole Streicher <olebole@debian.org>  Sat, 24 Jun 2017 13:46:06 +0200

wsclean (2.2.1-1) unstable; urgency=medium

  * New upstream version 2.2.1
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sun, 22 Jan 2017 11:17:31 +0100

wsclean (2.1-1) unstable; urgency=low

  * New upstream version 2.1
  * Rediff patches
  * Use default compilation on all architectures

 -- Ole Streicher <olebole@debian.org>  Mon, 19 Dec 2016 21:17:00 +0100

wsclean (2.0-2) unstable; urgency=medium

  * Enable SSE on i386

 -- Ole Streicher <olebole@debian.org>  Sat, 03 Dec 2016 15:14:14 +0100

wsclean (2.0-1) unstable; urgency=low

  * New upstream version 2.0
  * Rediff patches

 -- Ole Streicher <olebole@debian.org>  Sat, 05 Nov 2016 14:54:28 +0100

wsclean (1.12-6) unstable; urgency=medium

  * Use portable settings to build

 -- Ole Streicher <olebole@debian.org>  Thu, 20 Oct 2016 10:38:59 +0200

wsclean (1.12-5) unstable; urgency=medium

  * Fix dependencies of libwsclean0 and wsclean

 -- Ole Streicher <olebole@debian.org>  Thu, 20 Oct 2016 10:02:55 +0200

wsclean (1.12-4) unstable; urgency=low

  * Use libcfitsio-dev dependency. Closes: #841214

 -- Ole Streicher <olebole@debian.org>  Wed, 19 Oct 2016 09:15:57 +0200

wsclean (1.12-3) unstable; urgency=low

  * Add static lib to -dev
  * Limit to x86 architectures. Closes: #840037
  * Fix short description of lib and -dev
  * Make uscan more restrictive to exclude path names in version number

 -- Ole Streicher <olebole@debian.org>  Tue, 18 Oct 2016 16:09:46 +0200

wsclean (1.12-2) unstable; urgency=low

  * Build and use shared lib for libwsclean

 -- Ole Streicher <olebole@debian.org>  Fri, 30 Sep 2016 14:28:08 +0200

wsclean (1.12-1) unstable; urgency=low

  * Initial release. Closes: #830124

 -- Ole Streicher <olebole@debian.org>  Tue, 27 Sep 2016 23:28:26 +0200
